package net.babiran.app.Rss;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.babiran.app.R;

import java.util.List;


/**
 * Created by D on 11/2/2017.
 */

public class AdapterUserListToTo extends RecyclerView.Adapter<AdapterUserListToTo.MyViewHolder>
{
    private Context mContext;
    private List<BLOGME> listProducts;


    public AdapterUserListToTo(Context mContext, List<BLOGME> listProducts)
    {
        this.mContext = mContext;
        this.listProducts = listProducts;


        ;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_rec_rss_2, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {

        BLOGME list = listProducts.get(position);
        Picasso.with(mContext).load(list.image_link).into(holder.imageView);
        holder.SeenP.setText(list.titr);

    }

    @Override
    public int getItemCount() {
        return listProducts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView SeenP;
        ImageView imageView;



        public MyViewHolder(View view)
        {
            super(view);
            SeenP = (TextView) view.findViewById(R.id.txt_rc_rss2);

            imageView = (ImageView) view.findViewById(R.id.img_rc_rss2);


        }


    }

}
