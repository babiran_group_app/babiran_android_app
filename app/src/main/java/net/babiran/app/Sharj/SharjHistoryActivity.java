package net.babiran.app.Sharj;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.babiran.app.Actip2;
import net.babiran.app.MainActivity;
import net.babiran.app.R;
import net.babiran.app.Rss.AdapterUserList;
import net.babiran.app.Rss.ListRssActivity;
import net.babiran.app.Servic.MyInterFace;
import net.babiran.app.Servic.MyMesa;
import net.babiran.app.Servic.MyModelQu;
import net.babiran.app.Servic.MyServices;
import net.babiran.app.SharjActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Handlers.DatabaseHandler;
import Models.Product;
import retrofit2.Call;
import retrofit2.Callback;
import tools.AppConfig;

public class SharjHistoryActivity extends AppCompatActivity
{
    private   RecyclerView recyclerView;
    private    LinearLayoutManager linearLayoutManager;
    private List<String> list = new ArrayList<>();
   private AdapterHistorySharj mAdapter;
    DatabaseHandler db;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharj);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        db = new DatabaseHandler(getApplicationContext());
        HashMap<String, String> userDetailsHashMap = db.getUserDetails();
        id = Integer.parseInt(userDetailsHashMap.get("id"));
        Log.e("ID",""+id);

        INIT();
    }

    private void INIT()
    {
        recyclerView=(RecyclerView)findViewById(R.id.wfefwsfswf);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        SenDToServer();



//
    }







    private void SenDToServer()
    {


        try {
            MyInterFace n = MyServices.createService(MyInterFace.class);
            Call<List<MyModelQu>> call = n.HistorySharj(id);

            call.enqueue(new Callback<List<MyModelQu>>()
            {
                @Override
                public void onResponse(@NonNull Call<List<MyModelQu>> call, @NonNull retrofit2.Response<List<MyModelQu>> response)
                {

                    List<MyModelQu> s = response.body();
                    for(int i = 0 ; i< s.size() ; i++)
                    {
                        list.add(s.get(i).getMobile()+"##"+s.get(i).getOperatorId()+"##"+s.get(i).getPrice()+"##"+s.get(i).getCreatedAt()+"##"+s.get(i).getRefId());
                    }
                    mAdapter = new AdapterHistorySharj(SharjHistoryActivity.this, list);
                     recyclerView.setAdapter(mAdapter);

                }
                @Override
                public void onFailure(Call<List<MyModelQu>> call, Throwable t) {
                    Log.e("response 2  :", t.getMessage()+"\n"+t.toString());
                }
            });
        }catch (Exception ex)
        {
            Log.e("response 3 :", ex.getMessage());
        }


    }
}
